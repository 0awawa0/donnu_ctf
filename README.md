# DonNU CTF

There are tasks from trainings we maintain at Donetsk National University.

## 2020/2021

* [22.08.2020 - TestTraining](./2020_2021/TestTraining)

* [12.09.2020 - Training 1 MISC](./2020_2021/Training_1)

* [26.09.2020 - Training 2 Crypto](./2020_2021/Training_2)

* [10.10.2020 - Training 3 Reverse](./2020_2021/Training_3)

* [24.10.2020 - Training 4 Reverse](./2020_2021/Training_4)

* [07.11.2020 - Training 5 Reverse](./2020_2021/Training_5)

* [21.11.2020 - Training 6 Crypto](./2020_2021/Training_6)

* [05.12.2020 - Training 7 Forensic](./2020_2021/Training_7)

* [06.02.2021 - Training 8 MISC/Crypto](./2020_2021/Training_8)

* [21.02.2021 - Training 9 MISC](./2020_2021/Training_9)

* [20.03.2021 - Training 10 PWN](./2020_2021/Training_10)

* [10.04.2021 - Training 11 Forensic](./2020_2021/Training_11)

* [24.04.2021 - Training 12 WEB](./2020_2021/Training_12)

## 2021/2022

* [11.09.2021 - Training 1 Steganography](./2021_2022/Training_1)

* [25.09.2021 - Training 2 Symmetric Crypto](./2021_2022/Training_2)

* [09.10.2021 - Training 3 WEB](./2021_2022/Training_3)

* [23.10.2021 - Training 4 Asymmetric Crypto](./2021_2022/Training_4)

* [06.11.2021 - Training 5 Reverse](./2021_2022/Training_5)

* [20.11.2021 - Training 6 Reverse](./2021_2022/Training_6)