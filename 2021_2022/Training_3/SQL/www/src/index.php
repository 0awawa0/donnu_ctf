<!DOCTYPE html>
<html>
<head>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
        <div class="container-fluid">
            <h1 class="text-center">Can you finish all these SQL injection challenges?</h1>
        </div>

        <br>
        <br>
        <br>
        <br>

        <!-- Challenge #1 -->
        <div class="container-fluid">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Level 1</h3>
              </div>
              <div class="panel-body">
                My friend created a website where we can store secrets... Unfortunately, we can only see our own.
                Help me find all of my friend's secrets.
              </div>
              <div class="panel-footer">
    <a href="/level1.php" target="_blank" class="btn btn-success">Start here</a>
    <a href="/level1.php?source" target="_blank" class="btn btn-primary">Source code</a>

    </div>
            </div>
        </div>

        <!-- Challenge #2 -->
        <div class="container-fluid">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Level 2</h3>
              </div>
              <div class="panel-body">
                I think an administrator blocked my account. Can you help me steal someone else's account?
    <br/>
    <br/>
    <strong>Note</strong>: There are <u>two</u> flags in this challenge.
              </div>
              <div class="panel-footer">
    <a href="/level2.php" target="_blank" class="btn btn-warning">Start here</a>
    <a href="/level2.php?source" target="_blank" class="btn btn-primary">Source code</a>
    </div>
            </div>
        </div>

  <script
              src="https://code.jquery.com/jquery-3.1.1.min.js"
              integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
              crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>
